<?php

namespace App\Services\Stats\Providers;

use App\Services\Stats\Interfaces\SchoolStatsInterface;
use App\Services\Stats\Interfaces\StatsInterface;
use App\Services\Stats\Services\SchoolStatsService;
use App\Services\Stats\Services\StatsService;
use Illuminate\Support\ServiceProvider;

class StatsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StatsInterface::class, function ($app) {
            return new StatsService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Migrations');
    }
}
