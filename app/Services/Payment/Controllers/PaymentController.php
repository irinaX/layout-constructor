<?php


namespace App\Services\Payment\Controllers;


use App\Services\Payment\Interfaces\PaymentInterface;
use Illuminate\Http\Request;

class PaymentController
{
    public function resolveWebHook(Request $request, PaymentInterface $paymentInterface)
    {
        return $paymentInterface->resolveWebHook($request);
    }
}
