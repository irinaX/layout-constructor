<?php /** @noinspection PhpMissingFieldTypeInspection */

namespace App\Services\Payment\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['type', 'amount', 'credit', 'description', 'owner_type', 'owner_id'];

    public function owner()
    {
        return $this->morphTo('owner');
    }
}
