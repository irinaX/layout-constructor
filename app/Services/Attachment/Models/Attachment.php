<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Services\Attachment\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    protected $guarded = ['id'];

    const UPDATED_AT = null;

    protected $casts = [
        'info' => 'array',
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        if ($this->type != 'embed')
            return Storage::url($this->attributes['original'], $this->disk);
        else return $this->attributes['original'];
    }

    public function attachable()
    {
        return $this->morphTo();
    }
}
