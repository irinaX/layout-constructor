<?php


namespace App\Services\Settings\Services;


use App\Services\Settings\Models\Setting;

class SettingsService
{
    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return Setting::all()->mapWithKeys(function ($item) {
                return [$item['name'] => $item['value']];
            });

        } elseif (is_array($key)) {
            return Setting::whereIn('name', $key)->mapWithKeys(function ($item) {
                return [$item['name'] => $item['value']];
            });
        }

        $setting = Setting::where('name', $key)->first();
        return $setting ? $setting->value : $default;
    }

    public function set($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $name => $value) {
                Setting::where('name', $name)->update(['value' => $value]);
            }
            return;
        }

        Setting::where('name', $key)->update(['value' => $value]);
    }
}
