<?php


namespace App\Services\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = [];
    public static $useCasts = [];
    public $incrementing = false;
    public $timestamps = false;

    protected function getCastType($key)
    {
        return static::$useCasts[$this->attributes['name']];
    }

    public function hasCast($key, $types = null)
    {
        return $key == 'value' ? array_key_exists($this->attributes['name'], static::$useCasts) : false;
    }
}
