<?php

namespace App\Services\EmailVerification\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyEmailRedirectNotification extends Notification
{

    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Подтвердите регистрацию')
            ->line('Вы или кто-то другой указали эту почту в качестве основной на сайте razvivaites.ru. '
                . 'Пожалуйста, подтвердите регистрацию, нажав на кнопку ниже.')
            ->action('Подтвердить', $this->url)
            ->line('Просто проигнорируйте это письмо если вы не регистрировались на сайте.');
    }
}
