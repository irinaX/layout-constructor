export default class LayoutConstructor {
    constructor() {
        this.components = [];
        this.placeSubscribers = [];
    }

    place(component) {
        this.components.push(component);
        this.placeSubscribers.forEach(callback => callback(component))
    }

    subscribeToPlace(callback) {
        this.placeSubscribers.push(callback)
    }
}
