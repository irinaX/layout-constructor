import router from './router';
import App from './components/App';
import Vue from 'vue';


new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
