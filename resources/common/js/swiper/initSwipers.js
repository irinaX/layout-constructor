import Swiper from 'swiper/js/swiper.min';

window.Swiper = Swiper;

Swiper.instances = {};

// export function init() {
// }

export function init(swiper) {
    let preset = swiper.dataset.preset;
    let options;
    switch (preset) {
        case 'poster':
            options = {
                spaceBetween: 30,
                // autoplay: {
                //     delay: 2500,
                //     disableOnInteraction: false,
                // },
                loop: false,
                navigation: {
                    nextEl: '.swiper-poster-button-next',
                    prevEl: '.swiper-poster-button-prev',
                },
                pagination: {
                    el: '.swiper-poster-pagination',
                    type: 'bullets',
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1,
                    },
                    768: {
                        slidesPerView: 2,
                    },
                    1024: {
                        slidesPerView: 3,
                    },
                    1400: {
                        slidesPerView: 4,
                    },
                }
            };
            break;
        case 'popular-activities':
            options = {
                slidesPerView: 1,
                loop: true,
                // navigation: {
                //     nextEl: nextButton,
                //     prevEl: prevButton,
                // },
                navigation: {
                    prevEl: '.swiper-popular-button-prev',
                    nextEl: '.swiper-popular-button-next',
                },
                pagination: {
                    el: '.swiper-popular-pagination',
                    type: 'bullets',
                },
                observer: true
            };
            break;
        case 'blog':
            options = {
                loop: false,
                mousewheel: {invert: false, releaseOnEdges: true},
                direction: 'vertical',
                observer: true,
                slidesPerView: 3,
                navigation: {
                    nextButton: '.swiper-blog-button-next',
                },
                freeMode: true,
                spaceBetween: 60,
                breakpoints: {
                    769: {
                        slidesPerView: 5,
                        spaceBetween: 30
                    }
                }
            };
            break;
        case 'advert-activities':
            options = {
                loop: true,
                observer: true,
                slidesPerView: 1,
                navigation: {
                    nextButton: '.advert-activities-next',
                    prevButton: '.advert-activities-prev',
                },
                pagination: {
                    el: '.swiper-advert-pagination',
                    type: 'bullets',
                },
                spaceBetween: 30,
                breakpoints: {
                    769: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    }
                }
            };
            break;
        case 'scroll':
            options = {
                observer: true,
                observeParents: true,
                observeSlideChildren: true,
                direction: 'vertical',
                slidesPerView: 'auto',
                freeMode: true,
                mousewheel: {
                    releaseOnEdges: true,
                },
                scrollbar: {
                    el: '.swiper-scrollbar',
                },
            };
            break;
        case 'gallery':
            options = {
                observer: true,
                observeParents: true,
                observeSlideChildren: true,
                slidesPerView: 1,
                spaceBetween: 30,
                // mousewheel: {invert: false, releaseOnEdges: true},
                direction: 'vertical',
                freeMode: true,
                loop: false,
            };
            break;
        default:
            let slidesPerView = parseInt(swiper.dataset.slidesperview || 1);
            let spaceBetween = parseInt(swiper.dataset.spacebetween || 0);
            let nextButton = swiper.dataset.next;
            let prevButton = swiper.dataset.prev;
            let loop = swiper.dataset.loop !== 'false';
            let autoplay = swiper.dataset.autoplay;
            let paginationEl = swiper.dataset.paginationel;
            let paginationType = swiper.dataset.paginationtype || 'bullets';
            let direction = swiper.dataset.direction || 'horizontal';
            let name = swiper.dataset.name;
            let allowTouchMove = swiper.dataset.allowtouchmove !== 'false';
            let mousewheel = swiper.dataset.mousewheel ? {invert: false, releaseOnEdges: true} : false;
            options = {
                observer: true,
                observeParents: true,
                observeSlideChildren: true,
                slidesPerView: slidesPerView,
                spaceBetween: spaceBetween,
                allowTouchMove: allowTouchMove,
                direction: direction,
                mousewheel: mousewheel,
                loop: loop,
                ...(autoplay ? {
                    autoplay: {
                        delay: autoplay,
                        disableOnInteraction: true,
                    }
                } : {}),
                navigation: {
                    nextEl: nextButton,
                    prevEl: prevButton,
                },
                pagination: {
                    el: paginationEl,
                    type: paginationType,
                    clickable: true
                }
            };
            break;
    }
    // console.log(options);
    let sw = new Swiper(swiper, options);
    if (name) {
        Swiper.instances[name] = sw;
    }
}

export function initAll() {
    let swipers = document.getElementsByClassName('swiper-container');
    for (let i = 0; i < swipers.length; i++) {
        init(swipers[i]);
    }
    for (let i = 0; i < swipers.length; i++) {
        let name = swipers[i].dataset.name;
        let controlSwiper = swipers[i].dataset.controlswiper;

        if (controlSwiper) {
            Swiper.instances[name].controller.control = Swiper.instances[controlSwiper];
        }
    }
}
